#
# Copyright (C) 2017 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Plateforme Migale - MaIAGE'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '1.0'
__email__ = 'olivier.rue@inra.fr'

import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--dump', dest="dump", required=True, help='Tabular file containing list of merged taxids : from ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz / merged.dmp')
parser.add_argument('--file', dest="file", required=True, help='Tabular input file to update')
parser.add_argument('--taxid-col', dest="col", required=True, type=int, help='Column containing taxid to update')

args = parser.parse_args()

# my dictionnary for storing IDs to update
# {ID:new_ID}
dic={} 

# Read merged.dmp file to store IDs in my dictionnary
# The first column is the old ID, the second column is the new ID
h = open(args.dump,"r")
for line in h:
	tab = [i.rstrip().lstrip() for i in re.split("\|",line.rstrip())]
	dic[tab[0]]=tab[1]
h.close()

# Now read the file with IDs to change. The column containing the ID to check is given by parameter
# If needed, the ID is changed
# All lines are written to stdout
h = open(args.file,"r")
for line in h:
	tab = re.split("\t",line.rstrip())
	index = args.col-1
	if tab[index] in dic:
		tab[index]=dic[tab[index]]
	print("\t".join(tab))
h.close()

