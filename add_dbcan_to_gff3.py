#
# Copyright (C) 2016 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

author__ = 'Plateforme Migale - MaIAGE'
copyright__ = 'Copyright (C) 2022 INRAE'
license__ = 'GNU General Public License'
version__ = '1.0'
email__ = 'olivier.rue@inrae.fr'
status__ = 'beta'

from Bio.Seq import Seq
from gff3 import Gff3

import sys, os, argparse, re, shutil

############### ARGUMENTS 
parser = argparse.ArgumentParser(description="Add infos from DBCAN overview file in GFF3 file")
parser.add_argument('--gff', dest="gff_file", help='GFF3 file')
parser.add_argument('--gff-prodigal', dest="prodigal_gff_file", help='GFF3 file from prodigal')
parser.add_argument('--cgc-file', dest="cgc_file", help='File with CGC')
parser.add_argument('--overview', dest="overview_file", help='Overview file from DBCAN')
parser.add_argument('--output', dest="output_file", help='Output GFF3 file')
parser.add_argument('--min-nb-pred', dest="min_nb_pred", help='Min number of tools that found prediction', default=1, type=int)
args = parser.parse_args()


o = open(args.output_file,"w")

h = open(args.overview_file,"r")
dic_ov = {}
for line in h:
	if not line.startswith("Gene"):
		tab = re.split("\t",line.rstrip())
		if(int(tab[5]))>args.min_nb_pred:
			res = ""
			if tab[4] != "-":
				res=tab[4]
			elif tab[3] != "-":
				res=tab[3]
			else:
				res=tab[2]
			
			dic_ov[tab[0]]=res
			

dic_prodigal = {}
h = open(args.prodigal_gff_file,"r")
for line in h:
	if not line.startswith("#"):
		tab = re.split("\t",line.rstrip())
		#prodigal_id = re.split("_",re.split("=",re.split(";",tab[8])[0])[1])[1]
		#real_id = tab[0]+"_"+prodigal_id
		id=tab[0]+":"+tab[3]+"-"+tab[4]
		start_id = tab[0]+":0-"+tab[4]
		stop_id = tab[0]+":"+tab[3]+"-0"
		dic_prodigal[id]=tab
		dic_prodigal[start_id]=tab
		dic_prodigal[stop_id]=tab


dic_cgc = {}
h = open(args.cgc_file,"r")
for line in h:
	if not line.startswith("CGC#"):
		tab = re.split("\t",line.rstrip())
		dic_cgc[tab[3]]={}
		dic_cgc[tab[3]]["id"]=tab[0]
		dic_cgc[tab[3]]["type"]=tab[1]
		dic_cgc[tab[3]]["name"]=tab[7]


h = open(args.gff_file,"r")
for line in h:
	line = line.rstrip()
	if not line.startswith("#"):
		tab = re.split("\t",line.rstrip())
		if tab[2] == "CDS":
			suffix=""
			id=tab[0]+":"+tab[3]+"-"+tab[4]
			start_id = tab[0]+":0-"+tab[4]
			stop_id = tab[0]+":"+tab[3]+"-0"
			if id in dic_prodigal:
				prodigal_id = re.split("_",re.split("=",re.split(";",dic_prodigal[id][8])[0])[1])[1]
				real_id = dic_prodigal[id][0]+"_"+prodigal_id
				if real_id in dic_ov:
					suffix+=";dbcan="+dic_ov[real_id]
					del(dic_ov[real_id])
				if real_id in dic_cgc:
					suffix+=";cgc_id="+dic_cgc[real_id]["id"]
					suffix+=";cgc_type="+dic_cgc[real_id]["type"]
					suffix+=";cgc_name="+dic_cgc[real_id]["name"]
					del(dic_cgc[real_id])
				del(dic_prodigal[id])
			elif start_id in dic_prodigal:
				prodigal_id = re.split("_",re.split("=",re.split(";",dic_prodigal[start_id][8])[0])[1])[1]
				real_id = dic_prodigal[start_id][0]+"_"+prodigal_id
				if real_id in dic_ov:
					suffix+=";dbcan="+dic_ov[real_id]
					del(dic_ov[real_id])
				if real_id in dic_cgc:
					suffix+=";cgc_id="+dic_cgc[real_id]["id"]
					suffix+=";cgc_type="+dic_cgc[real_id]["type"]
					suffix+=";cgc_name="+dic_cgc[real_id]["name"]
					del(dic_cgc[real_id])
				del(dic_prodigal[start_id])
			elif stop_id in dic_prodigal:
				prodigal_id = re.split("_",re.split("=",re.split(";",dic_prodigal[stop_id][8])[0])[1])[1]
				real_id = dic_prodigal[stop_id][0]+"_"+prodigal_id
				if real_id in dic_ov:
					suffix+=";dbcan="+dic_ov[real_id]
					del(dic_ov[real_id])
				if real_id in dic_cgc:
					suffix+=";cgc_id="+dic_cgc[real_id]["id"]
					suffix+=";cgc_type="+dic_cgc[real_id]["type"]
					suffix+=";cgc_name="+dic_cgc[real_id]["name"]
					del(dic_cgc[real_id])
				del(dic_prodigal[stop_id])
				
			tab[-1]+=suffix
			o.write("\t".join(tab)+"\n")
				
		else:
			o.write(line+"\n")
	else:
		o.write(line+"\n")


print("Genes found by DBCAN and not found in GFF3 file\n")
for key, value in dic_ov.items():
    print(key, ' : ', value)
print("\n\n")
print("CGC with genes not found in GFF3 file\n")
for key, value in dic_cgc.items():
    print(key, ' : ', value)


